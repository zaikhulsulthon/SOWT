// [...Automated Watering System using STM32...]

#ifdef STM32L0
#include "stm32l0xx.h"
#define LEDPORT (GPIOB)
#define LED1 (3)
#define ENABLE_GPIO_CLOCK (RCC->IOPENR |= RCC_IOPENR_GPIOBEN)
#define _MODER MODER
#define GPIOMODER (GPIO_MODER_MODE3_0)
#elif STM32L1
#include "stm32l1xx.h"
#define LEDPORT (GPIOB)
#define LED1 (6)
#define ENABLE_GPIO_CLOCK (RCC->AHBENR |= RCC_AHBENR_GPIOBEN)
#define _MODER MODER
#define GPIOMODER (GPIO_MODER_MODER6_0)
#elif STM32L4
#include "stm32l4xx.h"
#define LEDPORT (GPIOB)
#define LED1 (3)
#define ENABLE_GPIO_CLOCK (RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN)
#define _MODER MODER
#define GPIOMODER (GPIO_MODER_MODE3_0)
#elif STM32L5
#include "stm32l5xx.h"
#define LEDPORT (GPIOB)
#define LED1 (6)
#define ENABLE_GPIO_CLOCK (RCC->AHBENR |= RCC_AHBENR_GPIOBEN)
#define _MODER MODER
#define GPIOMODER (GPIO_MODER_MODER6_0)
#elif STM32F0
#include "stm32f0xx.h"
#define LEDPORT (GPIOC)
#define LED1 (8)
#define ENABLE_GPIO_CLOCK (RCC->AHBENR |= RCC_AHBENR_GPIOCEN)
#define _MODER MODER
#define GPIOMODER (GPIO_MODER_MODER8_0)
#elif STM32F1
#include "stm32f1xx.h"
#define LEDPORT (GPIOC)
#define LED1 (13)
#define ENABLE_GPIO_CLOCK (RCC->APB2ENR |= RCC_APB2ENR_IOPCEN)
#define _MODER CRH
#define GPIOMODER (GPIO_CRH_MODE13_0)
#elif STM32F2
#include "stm32f2xx.h"
#define LEDPORT (GPIOB)
#define LED1 (0)
#define ENABLE_GPIO_CLOCK (RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN)
#define _MODER MODER
#define GPIOMODER (GPIO_MODER_MODER8_0)
#elif STM32F3
#include "stm32f3xx.h"
#define LEDPORT (GPIOE)
#define LED1 (8)
#define ENABLE_GPIO_CLOCK (RCC->AHBENR |= RCC_AHBENR_GPIOAEN)
#define _MODER MODER
#define GPIOMODER (GPIO_MODER_MODER8_0)
#elif STM32F4
#include "stm32f4xx.h"
#define LEDPORT (GPIOD)
#define LED1 (12)
#define ENABLE_GPIO_CLOCK (RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN)
#define _MODER MODER
#define GPIOMODER (GPIO_MODER_MODER12_0)
#elif STM32F7
#include "stm32f7xx.h"
#define LEDPORT (GPIOB)
#define LED1 (0)
#define ENABLE_GPIO_CLOCK (RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN)
#define _MODER MODER
#define GPIOMODER (GPIO_MODER_MODER0_0)
#elif STM32H7
#include "stm32h7xx.h"
#define LEDPORT (GPIOB)
#define LED1 (0)
#define ENABLE_GPIO_CLOCK (RCC->AHB4ENR |= RCC_AHB4ENR_GPIOBEN)
#define _MODER MODER
#define GPIOMODER (GPIO_MODER_MODE0_0)
#elif STM32G0
#include "stm32g0xx.h"
#define LEDPORT (GPIOA)
#define LED1 (5)
#define ENABLE_GPIO_CLOCK (RCC->IOPENR |= RCC_IOPENR_GPIOAEN)
#define _MODER MODER
#define GPIOMODER (GPIO_MODER_MODE5_0)
#elif STM32G4
#include "stm32g4xx.h"
#define LEDPORT (GPIOA)
#define LED1 (5)
#define ENABLE_GPIO_CLOCK (RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN)
#define _MODER MODER
#define GPIOMODER (GPIO_MODER_MODE5_0)
#endif

// #include "main.h"

char debug_array[20] = "";

// int i = 0;
// int speed = 0;
// int fan = 0;
// uint8_t intruder = 0;

void ms_delay(int ms)
{
    while (ms-- > 0)
    {
        volatile int x = 500;
        while (x-- > 0)
            __asm("nop");
    }
}

// /******imported variables and functions******/
// extern int timer1;
// extern int timer2;
// extern void Init_Timers(void);

// void automation_thread(void const *args)
// {
//     while (1)
//     {
//         osSignalWait(2, osWaitForever);
//         if (room.light1 == ON)
//             GPIO_WriteBit(GPIOB, GPIO_Pin_10, Bit_RESET);
//         if (room.light1 == OFF)
//             GPIO_WriteBit(GPIOB, GPIO_Pin_10, Bit_SET);
//         if (room.light2 == ON)
//             GPIO_WriteBit(GPIOB, GPIO_Pin_14, Bit_RESET);
//         if (room.light2 == OFF)
//             GPIO_WriteBit(GPIOB, GPIO_Pin_14, Bit_SET);

//         status.no_lights = room.light1 + room.light2;
//         status.no_fan = room.fan;

//         osSignalClear(thread_automation, 2);
//     }
// }

// osThreadDef(automation_thread, osPriorityRealtime, 1, 0);

// void fan_thread(void const *args)
// {

//     while (1)
//     {
//         // wait for signal
//         osSignalWait(3, osWaitForever);

//         if (room.fan != FAN0)
//         {
//             osDelay(speed);
//             GPIO_WriteBit(GPIOA, GPIO_Pin_8, Bit_SET);
//             // small delay of 10uS
//             for (i = 0; i < 400; i++)
//                 ;
//             GPIO_WriteBit(GPIOA, GPIO_Pin_8, Bit_RESET);
//         }
//         osSignalClear(thread_fan, 3);
//     }
// }
// osThreadDef(fan_thread, osPriorityRealtime, 1, 0);

// int main(void)
// {
//     // initialize CMSIS-RTOS
//     osKernelInitialize();

//     // initialize peripherals here
//     adc_init(ldr);
//     adc_init(temp);
//     init_RTC();
//     init_lights();
//     Init_Timers();
//     init_back();
//     init_room_struct();
//     init_USART6();
//     init_fan();

//     // create 'thread' functions that start executing,
//     osThreadCreate(osThread(main_thread), NULL);
//     thread_id = osThreadCreate(osThread(update_bars), NULL);
//     thread_automation = osThreadCreate(osThread(automation_thread), NULL);
//     osThreadCreate(osThread(polling_thread), NULL);
//     thread_fan = osThreadCreate(osThread(fan_thread), NULL);

//     osSignalSet(thread_id, 1);

//     // start thread execution
//     osKernelStart();
// }

// void EXTI0_IRQHandler(void)
// {
//     // Make sure the interrupt flag is set for EXTI0
//     if (EXTI_GetITStatus(EXTI_Line0) != RESET)
//     {
//         if (!back)
//         {
//             back = 1;
//         }
//         // Clear the interrupt flag
//         EXTI_ClearITPendingBit(EXTI_Line0);
//     }
// }
// void EXTI15_10_IRQHandler(void)
// {
//     /* Make sure that interrupt flag is set */
//     if (EXTI_GetITStatus(EXTI_Line15) != RESET)
//     {
//         /* Do your stuff when PB15 is changed */
//         if (room1.fan == 5)
//             GPIO_WriteBit(GPIOA, GPIO_Pin_8, Bit_SET);
//         else if (room1.fan == 0)
//             GPIO_WriteBit(GPIOA, GPIO_Pin_8, Bit_RESET);
//         else
//         {
//             GPIO_WriteBit(GPIOA, GPIO_Pin_8, Bit_RESET);
//             osSignalSet(thread_fan, 3);
//         }
//         EXTI_ClearITPendingBit(EXTI_Line15);
//     }
// }